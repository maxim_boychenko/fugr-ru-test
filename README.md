Как развернуть локально:

1. git clone https://maxim_boychenko@bitbucket.org/maxim_boychenko/fugr-ru-test.git
2. cd fugr-ru-test
3. composer install
4. прописать данные для соединения с базой данных в .env  
(для .env это DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name
, db_user - имя пользователя, db_password - пароль, db_name - имя базы данных, 
базу данных создавать не нужно, она будет создана на следующем шаге)
5. создание базы данных: bin/console d:d:c
6. выполнение миграции: bin/console d:migrations:m
7. запуск локального сервера: bin/console server:start
8. можно открыть страницу по адресу: localhost:8000