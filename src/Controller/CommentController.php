<?php

namespace App\Controller;

use App\Entity\Comment;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CommentController extends Controller
{
    /**
     * @Route("/", name="comment")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $comment = new Comment();

        $form = $this->createFormBuilder($comment)
            ->add('author', TextType::class, array(
                'label' => 'Ваше имя',
                'attr' => array('class' => 'author-form')
            ))
            ->add('text', TextareaType::class, array(
                'label' => 'Ваш комментарий',
                'attr' => array('class' => 'text-form')
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Отправить',
                'attr' => array('class' => 'button')
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Comment $comment */
            $comment = $form->getData();

            $comment->setCreated(new \DateTime);

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comment);
            $entityManager->flush();

            //return $this->redirectToRoute('task_success');
        }

        $comments = $this->getDoctrine()->getRepository(Comment::class)
            ->findBy(array(), array('created' => 'DESC'));

        return $this->render('comments.html.twig', array(
            'comments' => $comments,
            'form' => $form->createView(),
        ));
    }
}
